<?php 
namespace Sanipex\VisualMerch\Block\Adminhtml\Products;
use Magento\Store\Model\Store;

class Listing extends \Amasty\VisualMerch\Block\Adminhtml\Products\Listing {
    /**
     * @var \Amasty\VisualMerch\Model\Product\AdminhtmlDataProvider
     */
    private $dataProvider;

    
    private $resultIds = [];

   
    /**
     * @return $this
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Helper\Image $catalogImage,
        \Amasty\VisualMerch\Model\Product\AdminhtmlDataProvider $dataProvider,
        \Amasty\VisualMerch\Block\Adminhtml\Widget\Attribute\Factory $attributeFactory,
        \Magento\Framework\Registry $registry,
        \Magento\Store\Model\App\Emulation $emulation,
        array $data = []
    ) {
       
        $this->dataProvider = $dataProvider;
        parent::__construct($context, $backendHelper, $catalogImage, $dataProvider, $attributeFactory, $registry, $emulation, $data);
    }
    protected function _prepareCollection()
    {
        
        $collection = $this->dataProvider->getProductCollection();
        
        $this->setCollection($collection);
        
        $this->_preparePage();
        $collection->setPageSize(10000);
        $idx = ($collection->getCurPage() * $collection->getPageSize()) - $collection->getPageSize();
        
        foreach ($collection as $item) {
            $item->setPosition($idx);
            if (!empty($this->resultIds)) {
                $item->setIsSearchResult(in_array($item->getId(), $this->resultIds));
            }
            if (array_key_exists($item->getId(), $this->dataProvider->getProductPositionData())) {
                $item->setIsManual(true);
            }

            $idx++;
        }

        return $this;
    }
}

?>